import React, { useRef, useState } from "react";
import def, { ColorState, RGBColor } from "react-color";
import { SketchPicker } from "react-color";
import * as all from "react-color";
import { usePopper } from "react-popper";
import { useClickOutside } from "../util/clickOutside";
import { useRecoilState } from "recoil";
import { colorHistory } from "./toolState";
import clsx from "clsx";

console.info("Default:", def, all, SketchPicker);

export function formatColor(color: ColorState): string {
  return `rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${
    color.rgb.a ?? 1
  })`;
}

// this is as dirty as it gets
export function parseColor(value: string): RGBColor {
  const [r, g, b, a] = value
    .replace("rgba(", "")
    .replace(")", "")
    .split(", ")
    .map(parseFloat);
  return { r, g, b, a };
}

export interface ColorPickerProps {
  readonly value: string;
  readonly onChange: (color: string) => void;
  readonly label: string;
}

export const ColorPicker = ({ value, onChange, label }: ColorPickerProps) => {
  const [colors, setColors] = useRecoilState(colorHistory);
  const [color, setColor] = useState(value);

  const [open, setOpen] = useState(false);
  const referenceRef = useRef<HTMLButtonElement>(null);
  const [popperRef, setPopperRef] = useState<HTMLDivElement | null>(null);
  const arrowRef = useRef<HTMLDivElement>(null);

  const { styles, attributes } = usePopper(referenceRef.current, popperRef, {
    modifiers: [{ name: "arrow", options: { element: arrowRef.current } }],
  });

  useClickOutside(
    popperRef,
    () => {
      if (!open) {
        return;
      }
      setOpen(false);
      onChange(color);
      setColors((c) =>
        c.size < 16 ? c.add(color) : new Set([...c].slice(1).concat(color)),
      );
    },
    [color],
  );

  return (
    <>
      <div className="flex flex-col items-center">
        <button
          className={clsx(
            "w-12 h-12 p-2 mr-2 rounded-sm bg-specularity flex flex-row items-center justify-center",
          )}
          ref={referenceRef}
          onClick={() => setOpen((o) => !o)}
        >
          <div
            style={{ width: "80%", height: "80%", backgroundColor: "white" }}
          >
            <div
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: color,
                border: "1px solid #333",
              }}
            ></div>
          </div>
        </button>
        <label className="text-xs">{label}</label>
      </div>
      {open && (
        <div ref={setPopperRef} style={styles.popper} {...attributes.popper}>
          <SketchPicker
            color={parseColor(color)}
            onChangeComplete={(state) => {
              setColor(formatColor(state));
              onChange(formatColor(state));
            }}
            presetColors={[...colors].map((color) => ({ color, title: "" }))}
          />
          <div ref={arrowRef} style={styles.arrow}></div>
        </div>
      )}
    </>
  );
};
