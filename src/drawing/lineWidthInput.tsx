import React, { useState } from "react";
import { Input } from "../components/input";

export interface LineWidthInputProps {
  readonly lineWidth: number;
  readonly onChange: (v: number) => void;
}

export const LineWidthInput = ({
  lineWidth,
  onChange,
}: LineWidthInputProps) => {
  return (
    <Input
      className="self-start"
      type="number"
      onBlur={(value) => onChange(parseFloat(value))}
      value={"" + lineWidth}
    />
  );
};
