import { rectangleTool, circleTool, penTool, fillTool } from "./pen";
import { useRecoilState, useSetRecoilState } from "recoil";
import React from "react";
import { selectedStyle, selectedToolAtom } from "./toolState";
import { stepsAtom } from "./pictureState";
import { ColorPicker } from "./colorPicker";
import { LineWidthInput } from "./lineWidthInput";
import clsx from "clsx";

export interface ToolButtonProps {
  onClick: () => void;
  selected?: boolean;
  children: React.ReactNode;
}

export const ToolButton = (props: ToolButtonProps) => {
  return (
    <button
      className={clsx("w-12 h-12 p-2 mr-2 rounded-sm", {
        "bg-specularity": !props.selected,
        "bg-light-salmon shadow-neu": props.selected,
      })}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export const Toolbar = () => {
  const [selectedTool, setSelectedTool] = useRecoilState(selectedToolAtom);
  const [style, setStyle] = useRecoilState(selectedStyle);
  const updateSteps = useSetRecoilState(stepsAtom);

  return (
    <>
      <div className="mt-2 flex flex-row">
        <ToolButton
          onClick={() => setSelectedTool(rectangleTool)}
          selected={selectedTool === rectangleTool}
        >
          🔲
        </ToolButton>
        <ToolButton
          onClick={() => setSelectedTool(circleTool)}
          selected={selectedTool === circleTool}
        >
          ⭕
        </ToolButton>
        <ToolButton
          onClick={() => setSelectedTool(penTool)}
          selected={selectedTool === penTool}
        >
          🖌️
        </ToolButton>
        <ToolButton
          onClick={() => setSelectedTool(fillTool)}
          selected={selectedTool === fillTool}
        >
          🧯
        </ToolButton>
        <ToolButton
          onClick={() => updateSteps((s) => s.slice(0, s.length - 1))}
        >
          🔙
        </ToolButton>
      </div>
      <div className="mt-2 flex flex-row">
        <ColorPicker
          label="Fill"
          onChange={(color) => setStyle((s) => ({ ...s, fill: color }))}
          value={style.fill}
        />
        <ColorPicker
          label="Stroke"
          onChange={(color) => setStyle((s) => ({ ...s, stroke: color }))}
          value={style.stroke}
        />
        <LineWidthInput
          lineWidth={style.lineWidth}
          onChange={(value) => setStyle((s) => ({ ...s, lineWidth: value }))}
        />
      </div>
    </>
  );
};
