import { selector, atom, selectorFamily, atomFamily } from "recoil";
import type { DrawingStep } from "./pen";
import { selectedToolAtom } from "./toolState";

export const pictureSize = atom({
  key: "pictureSize",
  default: { x: 600, y: 400 },
});

export const offscreenBitmap = atomFamily<ImageData | undefined, string>({
  key: "offscreenBitmap",
  default: () => undefined,
  // ImageData can not be frozen
  dangerouslyAllowMutability: true,
});

export function getOffscreenCanvas(
  width: number,
  height: number,
): HTMLCanvasElement {
  const offscreenCanvas = document.createElement("canvas");
  offscreenCanvas.width = width;
  offscreenCanvas.height = height;
  return offscreenCanvas;
}

export const baseImage = selectorFamily<ImageData, string>({
  key: "baseImage",
  get: (id) => ({ get }) => {
    const { x, y } = get(pictureSize);
    const buffer = get(offscreenBitmap(id));
    const offscreenCanvas = getOffscreenCanvas(x, y);
    const ctx = offscreenCanvas.getContext("2d")!;
    if (buffer) {
      ctx.putImageData(buffer, 0, 0);
    } else {
      // always draw on white background, the default is transparent,
      // which messes up JPEG generation when saving.
      ctx.fillStyle = "white";
      ctx.fillRect(0, 0, x, y);
    }

    return ctx.getImageData(0, 0, x, y);
  },
  set: (id) => ({ set }, newValue) => {
    set(offscreenBitmap(id), newValue);
  },
  // ImageData can not be frozen
  dangerouslyAllowMutability: true,
});

export const stepsAtom = atom<DrawingStep[]>({
  key: "steps",
  default: [],
});

export const currentPicture = selectorFamily<ImageData, string>({
  key: "currentPicture",
  get: (id) => ({ get }) => {
    const { x, y } = get(pictureSize);
    const buffer = get(baseImage(id));

    const offscreenCanvas = getOffscreenCanvas(x, y);
    const ctx = offscreenCanvas.getContext("2d")!;

    ctx.putImageData(buffer, 0, 0);
    const steps = get(stepsAtom);

    for (const s of steps) {
      ctx.lineCap = "round";
      ctx.lineJoin = "round";
      ctx.fillStyle = s.style.fill;
      ctx.strokeStyle = s.style.stroke;
      ctx.lineWidth = s.style.lineWidth;

      ctx.beginPath();
      s.draw(ctx);
      ctx.stroke();
      ctx.fill();
    }

    return ctx.getImageData(0, 0, x, y);
  },
  // ImageData can not be frozen
  dangerouslyAllowMutability: true,
});
