import { atom } from "recoil";
import { rectangleTool, DrawingStyle, Tool } from "./pen";

export const selectedToolAtom = atom<Tool>({
  key: "selectedTool",
  default: rectangleTool,
  // we will mutate the tool, so we dangerously allow this. This does not need rerendering
  dangerouslyAllowMutability: true,
});

export const selectedStyle = atom<DrawingStyle>({
  key: "selectedStyle",
  default: {
    stroke: "black",
    lineWidth: 3,
    fill: "rgba(255, 0, 0, 0.5)",
  },
});

export const colorHistory = atom<Set<string>>({
  key: "colorHistory",
  default: new Set([
    "rgba(255, 0, 0, 1)",
    "rgba(0, 255, 0, 1)",
    "rgba(0, 0, 255, 1)",
    "rgba(255, 255, 0, 1)",
    "rgba(255, 0, 255, 1)",
    "rgba(0, 255, 255, 1)",
    "rgba(0, 0, 0, 1)",
    "rgba(255, 255, 255, 1)",
  ]),
});
