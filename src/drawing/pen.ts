import type { RGBColor } from "react-color";
import { parseColor } from "./colorPicker";

type RenderingContext = CanvasRenderingContext2D;

export interface DrawingStyle {
  readonly stroke: string;
  readonly fill: string;
  readonly lineWidth: number;
}

export interface DrawingStep {
  readonly style: DrawingStyle;

  draw(ctx: RenderingContext): void;
}

export interface Point {
  readonly x: number;
  readonly y: number;
}

export interface Tool {
  /**
   * down is called, when drawing starts
   * @param pos position where mouse is put down to start drawing
   */
  down(pos: Point): void;

  /**
   * move is called during movement of the mouse while painting
   * @param pos position where mouse is at
   */
  move(pos: Point): void;

  /**
   * paint is called when state is needed to be able to draw the tools output
   * @param pos position where mouse is at for current rendering step
   */
  paint(pos: Point, style: DrawingStyle): DrawingStep;
}

export class Rectangle implements DrawingStep {
  constructor(
    readonly a: Point,
    readonly b: Point,
    readonly style: DrawingStyle,
  ) {}

  draw(ctx: RenderingContext) {
    ctx.rect(this.a.x, this.a.y, this.b.x - this.a.x, this.b.y - this.a.y);
  }
}

export class Circle implements DrawingStep {
  constructor(
    readonly a: Point,
    readonly r: number,
    readonly style: DrawingStyle,
  ) {}

  draw(ctx: RenderingContext): void {
    ctx.ellipse(this.a.x, this.a.y, this.r, this.r, 0, 0, 2 * Math.PI);
  }
}

export class FreeHandLine implements DrawingStep {
  constructor(readonly points: Point[], readonly style: DrawingStyle) {}

  draw(ctx: RenderingContext): void {
    this.points.forEach((point) => ctx.lineTo(point.x, point.y));
  }
}

export class FloodFill implements DrawingStep {
  constructor(readonly pos: Point, readonly style: DrawingStyle) {}

  getColor(imageData: ImageData, pos: Point): RGBColor {
    const { x, y } = pos;
    const offset = 4 * (y * imageData.width + x);
    const r = imageData.data[offset];
    const g = imageData.data[offset + 1];
    const b = imageData.data[offset + 2];
    const a = imageData.data[offset + 3];

    return { r, g, b, a };
  }

  setColor(imageData: ImageData, pos: Point, color: string) {
    const { x, y } = pos;
    const offset = 4 * (y * imageData.width + x);
    const { r, g, b, a } = parseColor(color);

    imageData.data[offset] = r;
    imageData.data[offset + 1] = g;
    imageData.data[offset + 2] = b;
    imageData.data[offset + 3] = 255; // it makes surprisingly little sense to fill the alpha.
  }

  matchesColor(imageData: ImageData, pos: Point, color: RGBColor): boolean {
    const b = this.getColor(imageData, pos);

    return (
      color?.r === b.r &&
      color?.g === b.g &&
      color?.b === b.b &&
      color?.a === b.a
    );
  }

  draw(ctx: RenderingContext): void {
    const stack: Point[] = [{ ...this.pos }];
    const imageData = ctx.getImageData(
      0,
      0,
      ctx.canvas.width,
      ctx.canvas.height,
    );

    const initialColor = this.getColor(imageData, this.pos);
    while (stack.length) {
      let { x, y } = stack.pop()!;

      while (
        y >= 0 &&
        this.matchesColor(imageData, { x, y: y - 1 }, initialColor)
      ) {
        y--;
      }

      let reachLeft = false;
      let reachRight = false;

      while (
        y < ctx.canvas.height &&
        this.matchesColor(imageData, { x, y: y + 1 }, initialColor)
      ) {
        y++;
        this.setColor(imageData, { x, y }, this.style.fill);

        if (x > 0) {
          if (this.matchesColor(imageData, { x: x - 1, y }, initialColor)) {
            if (!reachLeft) {
              stack.push({ x: x - 1, y });
              reachLeft = true;
            }
          } else {
            reachLeft = false;
          }
        }

        if (x < ctx.canvas.width - 1) {
          if (this.matchesColor(imageData, { x: x + 1, y }, initialColor)) {
            if (!reachRight) {
              stack.push({ x: x + 1, y });
              reachRight = true;
            }
          } else {
            reachRight = false;
          }
        }
      }
    }

    ctx.putImageData(imageData, 0, 0);
  }
}

export class FillTool implements Tool {
  private pos: Point = { x: 0, y: 0 };

  down(pos: Point): void {
    this.pos = { x: Math.round(pos.x), y: Math.round(pos.y) };
  }
  move(pos: Point): void {
    // do nothing
  }
  paint(pos: Point, style: DrawingStyle): DrawingStep {
    return new FloodFill(this.pos, style);
  }
}

export class PenTool implements Tool {
  private points: Point[] = [];

  down(pos: Point): void {
    this.points = [];
    this.points.push(pos);
  }

  move(pos: Point): void {
    this.points.push(pos);
  }

  paint(pos: Point, style: DrawingStyle): DrawingStep {
    this.points.push(pos);
    const points = [...this.points];
    return new FreeHandLine(points, { ...style, fill: "transparent" });
  }
}

/**
 * SingleStepTool can draw all steps, which don't need intermediate state,
 * e.g. rectangles, circles, lines, etc.
 */
export class SingleStepTool implements Tool {
  private start: Point = { x: 0, y: 0 };
  private end: Point = { x: 0, y: 0 };

  constructor(
    private drawer: (
      start: Point,
      end: Point,
      style: DrawingStyle,
    ) => DrawingStep,
  ) {}

  down(pos: Point): void {
    this.start = pos;
  }

  move(pos: Point): void {
    this.end = pos;
  }

  paint(pos: Point, style: DrawingStyle): DrawingStep {
    this.end = pos;
    return this.drawer(this.start, this.end, style);
  }
}

export const rectangleTool = new SingleStepTool(
  (start, end, style) => new Rectangle(start, end, style),
);

export const circleTool = new SingleStepTool(
  (start, end, style) =>
    new Circle(
      start,
      Math.sqrt((start.x - end.x) ** 2 + (start.y - end.y) ** 2),
      style,
    ),
);

export const penTool = new PenTool();

export const fillTool = new FillTool();
