import React, { useRef, useEffect, useState } from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { selectedToolAtom, selectedStyle } from "./toolState";
import type { DrawingStep, Point } from "./pen";
import { currentPicture, stepsAtom, pictureSize } from "./pictureState";

export const Canvas = ({ id }: { id: string }) => {
  const canvas = useRef<HTMLCanvasElement>(null);
  const picture = useRecoilValue(currentPicture(id));

  const currentTool = useRecoilValue(selectedToolAtom);
  const style = useRecoilValue(selectedStyle);

  const [isDrawing, setIsDrawing] = useState(false);
  const [step, setStep] = useState<DrawingStep | undefined>();
  const updateSteps = useSetRecoilState(stepsAtom);

  const { x: width, y: height } = useRecoilValue(pictureSize);

  useEffect(() => {
    const ctx = canvas.current?.getContext("2d");
    if (ctx) {
      ctx.clearRect(
        0,
        0,
        canvas.current?.width ?? 0,
        canvas.current?.height ?? 0,
      );
      ctx.putImageData(picture, 0, 0);

      ctx.lineCap = "round";
      ctx.lineJoin = "round";
      ctx.fillStyle = step?.style?.fill ?? style.fill;
      ctx.strokeStyle = step?.style?.stroke ?? style.stroke;
      ctx.lineWidth = step?.style?.lineWidth ?? style.lineWidth;

      ctx.beginPath();

      step?.draw(ctx);

      ctx.stroke();
      ctx.fill();
    }
  }, [picture, step, style]);

  function getCoordinates(e: { clientX: number; clientY: number }): Point {
    const rect = canvas?.current?.getBoundingClientRect();
    return {
      x: e.clientX - (rect?.x ?? 0),
      y: e.clientY - (rect?.y ?? 0),
    };
  }

  return (
    <div>
      <canvas
        ref={canvas}
        className="border border-dark-purple shadow-neu"
        width={width}
        height={height}
        onMouseDown={(e) => {
          setIsDrawing(true);
          currentTool.down(getCoordinates(e));
        }}
        onTouchStart={(e) => {
          e.preventDefault();
          setIsDrawing(true);
          currentTool.down(getCoordinates(e.touches.item(0)));
        }}
        onMouseMove={(e) => {
          if (!isDrawing) {
            return;
          }
          const pos = getCoordinates(e);
          currentTool.move(pos);
          setStep(currentTool.paint(pos, style));
        }}
        onTouchMove={(e) => {
          e.preventDefault();

          if (!isDrawing) {
            return;
          }

          const pos = getCoordinates(e.touches.item(0));
          currentTool.move(pos);
          setStep(currentTool.paint(pos, style));
        }}
        onMouseUp={(e) => {
          setIsDrawing(false);
          setStep(undefined);
          updateSteps((s) => [
            ...s,
            currentTool.paint(getCoordinates(e), style),
          ]);
        }}
        onTouchEnd={(e) => {
          e.preventDefault();

          setIsDrawing(false);
          setStep(undefined);
          updateSteps((s) => [
            ...s,
            currentTool.paint(getCoordinates(e.changedTouches.item(0)), style),
          ]);
        }}
      ></canvas>
    </div>
  );
};
