import React from "react";
import { useRecoilState, useResetRecoilState, useSetRecoilState } from "recoil";
import { loggedInUsername } from "../authentication/playerState";
import { Input } from "../components/input";
import { activeGame, activeGameId } from "../game/gameState";
import { generateGameId } from "../game/game";
import { useHistory } from "react-router";
import { Button } from "../components/button";

export const Home = () => {
  const [username, setUsername] = useRecoilState(loggedInUsername);
  const createGame = useResetRecoilState(activeGame);
  const setActiveGameId = useSetRecoilState(activeGameId);
  const history = useHistory();

  return (
    <div className="shadow-neu rounded-lg p-8 m-8 h-full">
      <h2 className="text-lg mb-2">New Game</h2>
      <p className="mb-1">
        Please choose a nickname and start a new game, or wait for another
        player to share an invitation link with you.
      </p>
      <Input value={username} onBlur={setUsername} label="Nickname:" />
      <Button
        className="mt-2 w-full"
        onClick={() => {
          const gameId = generateGameId();
          setActiveGameId(gameId);
          createGame();
          history.push(`/game/${gameId}`);
        }}
      >
        New game!
      </Button>
    </div>
  );
};
