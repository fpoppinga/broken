import React, { useEffect } from "react";
import { Redirect, useParams } from "react-router";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  activeGame,
  joinLobby,
  activeGameId,
  newRound,
  activeRound,
} from "../game/gameState";
import { Lobby } from "../game/lobby";
import { loggedInPlayer } from "../authentication/playerState";
import { Spinner } from "../components/spinner";
import { GameView } from "../game/gameView";
import { GameStateView } from "../game/gameStateView";
import { Presentation } from "../game/presentation";
import { Button } from "../components/button";

export interface GamePageRouteParams {
  gameId: string;
}

export const Game = () => {
  const { gameId } = useParams<GamePageRouteParams>();
  const setActiveGameId = useSetRecoilState(activeGameId);
  useEffect(() => setActiveGameId(gameId), [gameId]);

  const game = useRecoilValue(activeGame);
  const player = useRecoilValue(loggedInPlayer);

  useEffect(() => {
    if (!game) {
      return;
    }
    joinLobby(game.id, player.id);
  }, [game?.id]);

  const round = useRecoilValue(activeRound);

  if (!player.nickname) {
    return <Redirect to="/" />;
  }

  if (!game) {
    return <Spinner />;
  }

  return (
    <div className="w-full px-8 max-w-6xl flex flex-col justify-between">
      <div>
        {!round && <Lobby game={game} />}
        <GameStateView game={game} />
        {round && <GameView game={game} />}
        {game.ownerId === player.id && !game.activeRound && (
          <Button
            className="w-full"
            disabled={game.players.size < 3}
            onClick={() => newRound(game)}
          >
            Start
          </Button>
        )}
        {round?.presentingBookId && (
          <Presentation
            bookId={round.presentingBookId}
            page={round.presentingPage ?? 0}
          />
        )}
      </div>
      <div className="mt-2">
        <a className="text-maroon-x11 underline" href={`/game/${gameId}`}>
          Game Link
        </a>
      </div>
    </div>
  );
};
