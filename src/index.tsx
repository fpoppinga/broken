import React, { Suspense, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { RecoilRoot, useRecoilValue, atom, selector } from "recoil";
import App from "./App";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import { Spinner } from "./components/spinner";
import "./styles.css";

export {};

const firebaseConfig = selector<Object>({
  key: "firebaseConfig",
  get: async () => {
    const configPath =
      import.meta.env.MODE === "production"
        ? "/__/firebase/init.json"
        : "/.firebase.config.json";

    const data = await fetch(configPath);
    return data.json();
  },
});

const InitFirebase = (props: { children: React.ReactNode }) => {
  const configObject = useRecoilValue(firebaseConfig);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    // this check is required for hot reloading
    if (firebase.apps.length === 0) {
      firebase.initializeApp(configObject);
    }
    setLoading(false);
  }, [configObject]);

  if (loading) {
    return <Spinner />;
  }

  return <>{props.children}</>;
};

ReactDOM.unstable_createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Suspense fallback={<Spinner />}>
      <RecoilRoot>
        <InitFirebase>
          <App />
        </InitFirebase>
      </RecoilRoot>
    </Suspense>
  </React.StrictMode>,
);

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://www.snowpack.dev/#hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept();
}
