import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { selector, atom, DefaultValue, selectorFamily } from "recoil";

export interface Player {
  readonly id: string;
  readonly nickname: string;
}

export const playerConverter: firebase.firestore.FirestoreDataConverter<Player> = {
  fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot): Player {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      nickname: data.nickname,
    };
  },
  toFirestore(data: Player): firebase.firestore.DocumentData {
    return { ...data };
  },
};

/**
 * userName is a read-write selector which auto-persists user name
 * changes in firestore
 */
export const loggedInUsername = selector<string>({
  key: "userName",
  get: ({ get }) => {
    const player = get(loggedInPlayer);
    return player.nickname;
  },
  set: ({ get }, newValue) => {
    if (newValue instanceof DefaultValue) {
      return;
    }

    const player = get(loggedInPlayer);
    return firebase
      .firestore()
      .doc(`players/${player.id}`)
      .withConverter(playerConverter)
      .set({ nickname: newValue }, { merge: true });
  },
});

/**
 * authenticatedUserId is the root atom for authentication
 */
export const authenticatedUserId = atom<string | null>({
  key: "authenticatedUserId",
  default: null,
});

/**
 * playerForId selects the player profile from firestore, given an userId
 */
export const playerForId = selectorFamily<Player | undefined, string>({
  key: "playerForId",
  get: (id) => async () => {
    const snapshot = await firebase
      .firestore()
      .doc(`players/${id}`)
      .withConverter(playerConverter)
      .get();

    return snapshot.data();
  },
});

export const loggedInPlayer = selector<Player>({
  key: "loggedInPlayer",
  get: async ({ get }) => {
    const userId = get(authenticatedUserId);

    if (!userId) {
      throw new Error("auth.missing");
    }

    const player = get(playerForId(userId));

    if (!player) {
      return {
        id: userId,
        nickname: "",
      };
    }

    return player;
  },
});
