import React, { useEffect, useState } from "react";
import firebase from "firebase/app";
import "firebase/auth";
import { useRecoilState } from "recoil";
import { authenticatedUserId } from "./playerState";
import { Spinner } from "../components/spinner";

export const Authentication = (props: { children: React.ReactNode }) => {
  const [userId, setUserId] = useRecoilState(authenticatedUserId);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      await firebase.auth().signInAnonymously();
      firebase.auth().onAuthStateChanged(
        (user) => {
          setLoading(false);
          if (!user) {
            setUserId(null);
            return;
          }

          setUserId(user.uid);
        },
        (err) => {
          console.warn("Authentication Error:", err);
          setLoading(false);
          setUserId(null);
        },
      );
    })();
  }, []);

  if (loading) {
    return <Spinner />;
  }

  if (!userId) {
    throw new Error("auth.failed");
  }

  return <>{props.children}</>;
};
