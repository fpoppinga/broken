import clsx from "clsx";
import React from "react";

export interface ButtonProps {
  className?: string;
  onClick?: () => void;
  disabled?: boolean;
  children?: React.ReactNode;
  alert?: boolean;
}

export const Button = ({
  onClick,
  alert,
  children,
  disabled,
  className,
}: ButtonProps) => {
  return (
    <button
      className={clsx(
        "px-4 py-2 font-bold rounded-sm shadow-neu",
        {
          "bg-crayola-green": !alert,
          "bg-light-salmon": alert,
          "text-opacity-25": disabled,
        },
        className,
      )}
      disabled={disabled}
      onClick={() => onClick?.()}
    >
      {children}
    </button>
  );
};
