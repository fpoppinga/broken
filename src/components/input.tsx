import clsx from "clsx";
import React, { useState } from "react";

export interface InputProps {
  onBlur?: (value: string) => void;
  value?: string;
  placeholder?: string;
  label?: string;
  className?: string;
  type?: string;
}

export const Input = (props: InputProps) => {
  const [value, setValue] = useState(props.value ?? "");

  return (
    <div className={clsx("flex flex-col", props.className)}>
      {props.label && (
        <label className="text-xs font-bold">{props.label}</label>
      )}
      <input
        className="border border-opacity-25 rounded-sm  px-1 pt-2 pb-1 shadow-inner text-dark-purple"
        value={value}
        onBlur={() => props.onBlur?.(value)}
        onChange={(e) => setValue(e.target.value)}
        placeholder={props.placeholder}
        type={props.type}
      />
    </div>
  );
};
