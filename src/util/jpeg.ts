import { getOffscreenCanvas } from "../drawing/pictureState";

export async function toJPEGBlob(imageData: ImageData): Promise<Blob | null> {
  const canvas = getOffscreenCanvas(imageData.width, imageData.height);
  const ctx = canvas.getContext("2d")!;
  ctx.putImageData(imageData, 0, 0);
  return new Promise((resolve) => canvas.toBlob(resolve, "image/jpeg", 0.95));
}
