import { useEffect } from "react";

export function useClickOutside<T extends HTMLElement>(
  element: T | null,
  onClickOutside: () => void,
  dependencies: any[] = [],
) {
  const handleClickOutside = (event: MouseEvent) => {
    if (!element?.contains(event.target as Node)) {
      onClickOutside();
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [element, ...dependencies]);
}
