/**
 * swap swaps two elements in an array mutably.
 * @param a the array
 * @param i first index
 * @param j second index
 */
function swap<T>(a: T[], i: number, j: number): T[] {
  const tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
  return a;
}

/**
 * rotations generates all rotations of an array a
 */
export function rotations<T>(a: T[]): Array<T[]> {
  const result: Array<T[]> = [];
  let mutable = [...a];
  for (let i = 0; i < a.length; i++) {
    const [first, ...rest] = mutable;
    mutable = [...rest, first];
    result.push(mutable);
  }

  return result;
}

/**
 * shuffle immutably shuffles an array using the Fisher-Yates algorithm:
 * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 * @param a the array
 */
export function shuffle<T>(a: T[]): T[] {
  const result = [...a];
  for (let i = result.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * result.length);
    swap(result, i, j);
  }

  return result;
}
