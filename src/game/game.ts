import type firebase from "firebase/app";
import {
  uniqueNamesGenerator,
  adjectives,
  animals,
  colors,
} from "unique-names-generator";

export interface Game {
  readonly id: string;
  readonly created: Date;
  readonly ownerId: string;
  readonly players: Set<string>;
  readonly lobby: Set<string>;
  readonly activeRound: string | null;
}

export const gameConverter: firebase.firestore.FirestoreDataConverter<Game> = {
  fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot): Game {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      created: data.created,
      ownerId: data.ownerId,
      activeRound: data.activeRound,
      players: new Set(data.players),
      lobby: new Set(data.lobby),
    };
  },
  toFirestore(data: Game): firebase.firestore.DocumentData {
    return {
      ...data,
      players: [...data.players],
      lobby: [...data.lobby],
    };
  },
};

export function generateGameId(): string {
  return uniqueNamesGenerator({
    dictionaries: [adjectives, colors, animals],
    length: 3,
    separator: "-",
  });
}

export interface Round {
  readonly id: string;
  readonly playerIds: Set<string>;
  readonly presentingBookId: string | null;
  readonly presentingPage: number | null;
}

export const roundConverter: firebase.firestore.FirestoreDataConverter<Round> = {
  fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot): Round {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      playerIds: new Set(data.playerIds),
      presentingBookId: data.presentingBookId ?? null,
      presentingPage: data.presentingPage ?? null,
    };
  },
  toFirestore(data: Round): firebase.firestore.DocumentData {
    return {
      playerIds: [...data.playerIds],
    };
  },
};

export type PageType = "picture" | "description";

export interface Book {
  readonly id: string;
  readonly playerId: string;
  readonly currentPage: number | null;
  readonly schedule: Array<[string, PageType]>;
}

export const bookConverter: firebase.firestore.FirestoreDataConverter<Book> = {
  fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot): Book {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      playerId: data.playerId,
      currentPage: data.currentPage,
      schedule: data.schedule.map((it: string) => it.split(":")),
    };
  },
  toFirestore(data: Book): firebase.firestore.DocumentData {
    return {
      ...data,
      schedule: data.schedule.map((it) => it.join(":")),
    };
  },
};

export type Page = PicturePage | DescriptionPage;

export interface PicturePage {
  readonly id: string;
  readonly type: "picture";
  readonly roundId: string;
  readonly bookId: string;
  readonly index: number;
  readonly author: string;
  readonly imageHref: string | null;
}

export interface DescriptionPage {
  readonly id: string;
  readonly type: "description";
  readonly roundId: string;
  readonly bookId: string;
  readonly index: number;
  readonly author: string;
  readonly description: string | null;
}

export const pageConverter: firebase.firestore.FirestoreDataConverter<Page> = {
  fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot): Page {
    const data = snapshot.data();
    if (data.type === "picture") {
      return {
        id: snapshot.id,
        type: data.type,
        roundId: data.roundId,
        bookId: data.bookId,
        index: data.index,
        author: data.author,
        imageHref: data.imageHref,
      };
    }

    return {
      id: snapshot.id,
      type: data.type,
      roundId: data.roundId,
      bookId: data.bookId,
      index: data.index,
      author: data.author,
      description: data.description,
    };
  },
  toFirestore(data: Page): firebase.firestore.DocumentData {
    return {
      ...data,
    };
  },
};
