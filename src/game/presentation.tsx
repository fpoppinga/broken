import React from "react";
import { useRecoilValue, waitForAll } from "recoil";
import { isDefined } from "../util/isDefined";
import { loggedInPlayer, playerForId } from "../authentication/playerState";
import type { Page } from "./game";
import {
  activeGame,
  bookForId,
  incrementPresentationPage,
  pageInBook,
} from "./gameState";
import { DescriptionPageView, PicturePageView } from "./gameView";
import { Button } from "../components/button";

export interface PresentationProps {
  readonly bookId: string;
  readonly page: number;
}

function range(x: number): number[] {
  const result = [];
  for (let i = 0; i <= x; i++) {
    result.push(i);
  }
  return result;
}

export const Presentation = ({ bookId, page }: PresentationProps) => {
  const game = useRecoilValue(activeGame);
  const user = useRecoilValue(loggedInPlayer);
  const isOwner = game?.ownerId === user.id;

  const book = useRecoilValue(bookForId(bookId));
  const author = useRecoilValue(playerForId(book?.playerId ?? ""));

  const pages: Page[] = useRecoilValue(
    waitForAll(range(page).map((i) => pageInBook([bookId, i]))) as any,
  );

  return (
    <div>
      <h2 className="text-xl">
        Presentation: <span className="text-bold">{author?.nickname}'s</span>{" "}
        Book
      </h2>
      {isOwner && (
        <div className="flex flex-row justify-between">
          <Button
            disabled={page === 0}
            onClick={() => game && incrementPresentationPage(game, -1)}
          >
            ⬅️
          </Button>
          <Button
            disabled={page + 1 === (book?.schedule.length ?? 0)}
            onClick={() => game && incrementPresentationPage(game, 1)}
          >
            ➡️
          </Button>
        </div>
      )}
      {pages
        .filter(isDefined)
        .reverse()
        .map((it) =>
          it.type === "description" ? (
            <DescriptionPageView key={it.id} page={it} />
          ) : (
            <PicturePageView key={it.id} page={it} />
          ),
        )}
    </div>
  );
};
