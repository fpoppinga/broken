import React, { useState } from "react";
import type {
  Game,
  DescriptionPage,
  PicturePage,
  Page,
  PageType,
} from "./game";
import { useRecoilValue } from "recoil";
import {
  activePages,
  pageInBook,
  bookForId,
  submitDescription,
  activeGame,
  submitPicture,
} from "./gameState";
import { playerForId } from "../authentication/playerState";
import { Input } from "../components/input";
import { Canvas } from "../drawing/canvas";
import { Toolbar } from "../drawing/toolbar";
import { useRecoilCallback } from "recoil";
import { currentPicture } from "../drawing/pictureState";
import { Button } from "../components/button";

export interface GameViewProps {
  readonly game: Game;
}

export const GameView = (props: GameViewProps) => {
  const pages = useRecoilValue(activePages);

  return (
    <div>
      {pages.map((it) => (
        <PageView key={it.id} page={it} />
      ))}
    </div>
  );
};

interface PageViewProps {
  readonly page: Page;
}

export const PageView = ({ page }: PageViewProps) => {
  const game = useRecoilValue(activeGame)!;
  const predecessor = useRecoilValue(pageInBook([page.bookId, page.index - 1]));
  const book = useRecoilValue(bookForId(page.bookId));
  const [error, setError] = useState<Error | undefined>(undefined);

  if (!book) {
    throw new Error("book.missing");
  }

  const owner = useRecoilValue(playerForId(book?.playerId));
  const submitImage = useRecoilCallback(
    ({ snapshot }) => async (id: string) => {
      if (page.type !== "picture") {
        return;
      }

      const data = await snapshot.getPromise(currentPicture(id));
      await submitPicture(game, book, page, data);
    },
    [page.id],
  );

  const currentTask: PageType = page.type;

  const descriptionPage: DescriptionPage | null =
    page.type === "description"
      ? page
      : (predecessor as DescriptionPage | null);
  const picturePage: PicturePage | null =
    page.type === "picture" ? page : (predecessor as PicturePage | null);

  return (
    <div>
      <h2 className="mt-8">
        {owner?.nickname}'s Book, Page: {page.index + 1}
      </h2>
      {descriptionPage && (
        <DescriptionPageView
          edit={currentTask === "description"}
          page={descriptionPage}
          onSubmit={async (description) => {
            console.info("submit ", description);
            try {
              await submitDescription(game, book, {
                ...descriptionPage,
                description,
              });
              setError(undefined);
            } catch (e) {
              setError(e);
            }
          }}
        />
      )}
      {picturePage && (
        <PicturePageView
          edit={currentTask === "picture"}
          page={picturePage}
          onSubmit={async (id) => {
            console.info("submit ", id);
            try {
              await submitImage(id);
              setError(undefined);
            } catch (e) {
              setError(e);
            }
          }}
        />
      )}
      {error && <p className="text-maroon-x11">{error.message}</p>}
    </div>
  );
};

interface DescriptionPageProps {
  readonly page: DescriptionPage;
  readonly edit?: boolean;
  readonly onSubmit?: (description: string) => void;
}

export const DescriptionPageView = (props: DescriptionPageProps) => {
  const [description, setDescription] = useState(props.page.description ?? "");

  return (
    <form
      className="flex"
      onSubmit={(e) => {
        e.preventDefault();
        props.onSubmit?.(description);
      }}
    >
      <div className="flex-grow">
        {props.edit ? (
          <Input
            value={description}
            label="Description:"
            placeholder={"Come up with a good description..."}
            onBlur={setDescription}
          />
        ) : (
          <h2 className="text-xl">{description}</h2>
        )}
      </div>
      {props.edit && <Button className="ml-2 self-end">Submit</Button>}
    </form>
  );
};

interface PicturePageProps {
  readonly page: PicturePage;
  readonly edit?: boolean;
  readonly onSubmit?: (id: string) => void;
}

export const PicturePageView = ({ edit, page, onSubmit }: PicturePageProps) => {
  return (
    <div className="mt-2">
      <div className="flex flex-row justify-center">
        {edit ? (
          <div>
            <Canvas id={page.id} />
            <Toolbar />
          </div>
        ) : (
          page.imageHref && (
            <div>
              <img
                className="border border-dark-purple shadow-neu"
                src={page.imageHref}
              />
            </div>
          )
        )}
      </div>

      {edit && (
        <Button className="w-full" onClick={() => onSubmit?.(page.id)}>
          Submit
        </Button>
      )}
    </div>
  );
};
