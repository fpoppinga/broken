import { useRecoilValue, useSetRecoilState } from "recoil";
import { useEffect } from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import {
  activeGameId,
  activeGameAtom,
  activeGame,
  activePages,
  activeBooks,
  activeRound,
} from "./gameState";
import {
  bookConverter,
  gameConverter,
  pageConverter,
  roundConverter,
} from "./game";
import { loggedInPlayer } from "../authentication/playerState";
import { isDefined } from "../util/isDefined";

export const GameLiveUpdate = () => {
  const gameId = useRecoilValue(activeGameId);
  const setActiveGame = useSetRecoilState(activeGameAtom);

  useEffect(() => {
    return firebase
      .firestore()
      .doc(`games/${gameId}`)
      .withConverter(gameConverter)
      .onSnapshot((snapshot) => {
        const data = snapshot.data();
        setActiveGame(data);
      });
  }, [gameId]);

  return null;
};

export const ActivePageLiveUpdate = () => {
  const game = useRecoilValue(activeGame);
  const user = useRecoilValue(loggedInPlayer);
  const setActivePages = useSetRecoilState(activePages);
  const setActiveBooks = useSetRecoilState(activeBooks);
  const setActiveRound = useSetRecoilState(activeRound);

  // round live update
  useEffect(() => {
    if (!game || !game.activeRound) {
      return () => void {};
    }

    return firebase
      .firestore()
      .doc(`games/${game.id}/rounds/${game.activeRound}`)
      .withConverter(roundConverter)
      .onSnapshot((snapshot) => {
        setActiveRound(snapshot.data());
      });
  });

  // game book live update
  useEffect(() => {
    if (!game || !game.activeRound) {
      return () => void {};
    }

    return firebase
      .firestore()
      .collection(`games/${game.id}/rounds/${game.activeRound}/books`)
      .withConverter(bookConverter)
      .onSnapshot((snapshot) => {
        const data = snapshot.docs.map((it) => it.data()).filter(isDefined);
        setActiveBooks(data);
      });
  });

  // page live update
  useEffect(() => {
    if (!game || !game.activeRound) {
      return () => void {};
    }

    return firebase
      .firestore()
      .collectionGroup("pages")
      .withConverter(pageConverter)
      .where("roundId", "==", game.activeRound)
      .where("author", "==", user.id)
      .orderBy("index", "desc")
      .onSnapshot((snapshot) => {
        const pages = snapshot.docs.map((snap) => snap.data());
        setActivePages(
          pages.filter((it) => {
            return it.type === "description" ? !it.description : !it.imageHref;
          }),
        );
      });
  }, [game?.activeRound, user.id]);

  return null;
};
