import React from "react";
import { useRecoilValue, waitForAll } from "recoil";
import type { Book, Game } from "./game";
import {
  activeBooks,
  isRoundComplete,
  startPresentation,
  stopRound,
} from "./gameState";
import {
  loggedInPlayer,
  Player,
  playerForId,
} from "../authentication/playerState";
import type { RecoilValueReadOnly } from "recoil";
import { Button } from "../components/button";

interface GameStateViewProps {
  readonly game: Game;
}

export const GameStateView = ({ game }: GameStateViewProps) => {
  const books = useRecoilValue(activeBooks);
  const user = useRecoilValue(loggedInPlayer);
  const roundComplete = useRecoilValue(isRoundComplete);

  const showPresentationControls = roundComplete && user.id === game.ownerId;

  const selectors = books.reduce(
    (
      acc: { [id: string]: RecoilValueReadOnly<Player | undefined> },
      it: Book,
    ) => ({
      ...acc,
      [it.playerId]: playerForId(it.playerId),
    }),
    {},
  );

  const players = useRecoilValue<{ [id: string]: Player }>(
    waitForAll(selectors) as any,
  );

  return (
    <div className="">
      <div className="my-2 flex justify-center">
        {game.activeRound ? (
          roundComplete ? (
            <div>
              <p>Round complete!</p>
              {user.id === game.ownerId && (
                <Button alert onClick={() => stopRound(game.id)}>
                  Next Round!
                </Button>
              )}
            </div>
          ) : (
            <p>Game in Progress</p>
          )
        ) : (
          <p>Waiting for owner to start the game.</p>
        )}
      </div>
      <ul>
        {books.map((book) => {
          const bookOwner = players[book.playerId];
          const [activePlayerId, task] =
            book.schedule[book.currentPage ?? -1] ?? [];
          const bookDone = book.currentPage === book.schedule.length;
          const activePlayer = players[activePlayerId];
          return (
            <li
              className="flex flex-row py-2 justify-between items-center border-b border-dark-purple border-opacity-25"
              key={book.playerId}
            >
              <div>
                <span className="font-bold">{bookOwner?.nickname}'s</span> Book:{" "}
              </div>
              <div>
                {bookDone ? (
                  "done!"
                ) : (
                  <div>
                    <span className="font-bold">{activePlayer?.nickname}</span>
                    {` at page ${book.currentPage} (${task})`}
                  </div>
                )}
                {showPresentationControls && (
                  <Button
                    className="ml-4"
                    onClick={() => startPresentation(game, book.id)}
                  >
                    Start Presentation
                  </Button>
                )}
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
