import { atom, DefaultValue, selector, selectorFamily } from "recoil";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import {
  Game,
  gameConverter,
  Book,
  roundConverter,
  bookConverter,
  PageType,
  pageConverter,
  Page,
  DescriptionPage,
  PicturePage,
  Round,
} from "./game";
import { loggedInPlayer } from "../authentication/playerState";
import { rotations, shuffle } from "../util/permutations";
import { toJPEGBlob } from "../util/jpeg";

export const activeGameId = atom<string | undefined>({
  key: "activeGameId",
  default: undefined,
});

export const activeGameAtom = atom<Game | undefined>({
  key: "activeGameAtom",
  default: undefined,
});

export const activeGame = selector<Game | undefined>({
  key: "gameForId",
  get: async ({ get }) => {
    const liveUpdated = get(activeGameAtom);
    if (liveUpdated) {
      return liveUpdated;
    }

    get(activeGameId);
    if (!activeGameId) {
      return undefined;
    }

    const snapshot = await firebase
      .firestore()
      .doc(`games/${activeGameId}`)
      .withConverter(gameConverter)
      .get();
    return snapshot.data();
  },
  set: async ({ get }, newGame) => {
    const id = get(activeGameId);
    if (!id) {
      return;
    }

    if (!newGame || newGame instanceof DefaultValue) {
      const ownerId = get(loggedInPlayer).id;

      const game: Game = {
        id,
        created: new Date(),
        ownerId,
        lobby: new Set(),
        players: new Set([ownerId]),
        activeRound: null,
      };

      await firebase
        .firestore()
        .doc(`games/${id}`)
        .withConverter(gameConverter)
        .set(game);
    }
  },
});

export const activePages = atom<Page[]>({
  key: "activePages",
  default: [],
});

export const activeRound = atom<Round | undefined>({
  key: "activeRound",
  default: undefined,
});

export const pageInBook = selectorFamily<Page | null, [string, number]>({
  key: "pageInBook",
  get: ([bookId, pageIndex]) => async ({ get }) => {
    const game = get(activeGame);

    if (!game) {
      return null;
    }

    console.info("get page", bookId, pageIndex);

    const snapshot = await firebase
      .firestore()
      .collection(
        `games/${game.id}/rounds/${game.activeRound}/books/${bookId}/pages`,
      )
      .withConverter(pageConverter)
      .where("index", "==", pageIndex)
      .limit(1)
      .get();

    return snapshot.docs[0]?.data() ?? null;
  },
});

export const bookForId = selectorFamily<Book | undefined, string>({
  key: "bookForId",
  get: (bookId) => async ({ get }) => {
    const game = get(activeGame);

    if (!game) {
      return undefined;
    }

    const snapshot = await firebase
      .firestore()
      .doc(`games/${game.id}/rounds/${game.activeRound}/books/${bookId}`)
      .withConverter(bookConverter)
      .get();

    return snapshot.data();
  },
});

export const activeBooks = atom<Book[]>({
  key: "activeBooks",
  default: [],
});

export const isRoundComplete = selector<boolean>({
  key: "isRoundComplete",
  get: ({ get }) => {
    const books = get(activeBooks);

    return (
      books.length > 0 &&
      books.every((it) => it.currentPage === it.schedule.length)
    );
  },
});

// ---
// Actions
// ---
export function joinLobby(gameId: string, playerId: string): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${gameId}`)
    .update({
      lobby: firebase.firestore.FieldValue.arrayUnion(playerId),
    });
}

export function kickPlayer(gameId: string, playerId: string): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${gameId}`)
    .update({
      lobby: firebase.firestore.FieldValue.arrayRemove(playerId),
      players: firebase.firestore.FieldValue.arrayRemove(playerId),
    });
}

export function makePlayer(gameId: string, playerId: string): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${gameId}`)
    .update({
      players: firebase.firestore.FieldValue.arrayUnion(playerId),
    });
}

export async function newRound(game: Game): Promise<void> {
  // Highly Complex Scheduling:
  //
  // Goals:
  // - every player has to do every page number exactly once
  // - people are not always in the same order (i.e. player A
  //   does not always get the book player B had before).
  // - players do not get their own book, other than the first page
  // - players do not get the same book twice
  //
  // Solution:
  // - take the vector of players [a, b, c, d, e, f] for the first page
  // - create all rotations
  // - in random order, use each rotations for one page

  const [initial, ...rest] = rotations([...game.players]);
  const shuffled = shuffle(rest);
  const books: Book[] = initial.map((id, bookIdx) => ({
    id: "", // will be auto-generated
    playerId: id,
    schedule: [
      // first page
      [id, "description"],
      // next pages
      ...shuffled.map(
        (a, pageIdx) =>
          [a[bookIdx], pageIdx % 2 === 0 ? "picture" : "description"] as [
            string,
            PageType,
          ],
      ),
    ],
    currentPage: 0,
  }));

  const roundRef = await firebase
    .firestore()
    .collection(`games/${game.id}/rounds`)
    .withConverter(roundConverter)
    .add({
      id: "", // will be auto-generated
      playerIds: game.players,
      presentingBookId: null,
      presentingPage: null,
    });

  const batch = firebase.firestore().batch();
  books.forEach((it) => {
    const bookRef = roundRef
      .collection("books")
      .withConverter(bookConverter)
      .doc();
    batch.set(bookRef, it);

    // create first page
    batch.set(bookRef.collection(`pages`).withConverter(pageConverter).doc(), {
      id: "", // will be auto-generated
      author: it.playerId,
      type: "description",
      index: 0,
      description: null,
      roundId: roundRef.id,
      bookId: bookRef.id,
    });
  });
  await batch.commit();

  await firebase
    .firestore()
    .doc(`games/${game.id}`)
    .withConverter(gameConverter)
    .update({
      activeRound: roundRef.id,
    });
}

export async function submitDescription(
  game: Game,
  book: Book,
  page: DescriptionPage,
) {
  if (!page.description) {
    throw new Error("description.missing");
  }

  if (!game.activeRound) {
    throw new Error("activeRound.missing");
  }

  const [nextPlayer, nextPageType] = book.schedule[page.index + 1] ?? [];

  const batch = firebase.firestore().batch();

  const bookRef = firebase
    .firestore()
    .doc(`games/${game.id}/rounds/${game.activeRound}/books/${page.bookId}`);

  batch.update(bookRef.collection("pages").doc(page.id), {
    description: page.description,
  });

  batch.update(bookRef, {
    currentPage: firebase.firestore.FieldValue.increment(1),
  });

  if (nextPlayer && nextPageType) {
    batch.set(
      firebase
        .firestore()
        .collection(
          `games/${game.id}/rounds/${game.activeRound}/books/${page.bookId}/pages`,
        )
        .withConverter(pageConverter)
        .doc(),
      {
        id: "", // will be auto-generated
        type: nextPageType,
        author: nextPlayer,
        bookId: page.bookId,
        roundId: game.activeRound,
        index: page.index + 1,
        description: null,
        imageHref: null,
      },
    );
  }

  await batch.commit();
}

export async function submitPicture(
  game: Game,
  book: Book,
  page: PicturePage,
  data: ImageData,
) {
  if (!game.activeRound) {
    throw new Error("activeRound.missing");
  }

  const [nextPlayer, nextPageType] = book.schedule[page.index + 1] ?? [];

  const jpeg = await toJPEGBlob(data);
  if (!jpeg) {
    throw new Error("image.conversion.faile");
  }

  const uploadResult = await firebase.storage().ref(page.id).put(jpeg);
  const imageHref = await uploadResult.ref.getDownloadURL();

  const bookRef = firebase
    .firestore()
    .doc(`games/${game.id}/rounds/${game.activeRound}/books/${page.bookId}`);

  const batch = firebase.firestore().batch();

  batch.update(bookRef.collection("pages").doc(page.id), { imageHref });

  batch.update(bookRef, {
    currentPage: firebase.firestore.FieldValue.increment(1),
  });

  if (nextPlayer && nextPageType) {
    batch.set(
      firebase
        .firestore()
        .collection(
          `games/${game.id}/rounds/${game.activeRound}/books/${page.bookId}/pages`,
        )
        .withConverter(pageConverter)
        .doc(),
      {
        id: "", // will be auto-generated
        type: nextPageType,
        author: nextPlayer,
        bookId: page.bookId,
        roundId: game.activeRound,
        index: page.index + 1,
        description: null,
        imageHref: null,
      },
    );
  }

  await batch.commit();
}

export function startPresentation(game: Game, bookId: string): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${game.id}/rounds/${game.activeRound}`)
    .update({
      presentingBookId: bookId,
      presentingPage: 0,
    });
}

export function stopPresentation(game: Game): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${game.id}/rounds/${game.activeRound}`)
    .update({
      presentingBookId: null,
      presentingPage: null,
    });
}

export function incrementPresentationPage(
  game: Game,
  incrementBy: number,
): Promise<void> {
  return firebase
    .firestore()
    .doc(`games/${game.id}/rounds/${game.activeRound}`)
    .update({
      presentingPage: firebase.firestore.FieldValue.increment(incrementBy),
    });
}

export function stopRound(gameId: string): Promise<void> {
  return firebase.firestore().doc(`games/${gameId}`).update({
    activeRound: null,
  });
}
