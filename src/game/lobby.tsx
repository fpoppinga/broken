import type { Game } from "./game";
import React from "react";
import { useRecoilValue, waitForAll } from "recoil";
import {
  playerForId,
  Player,
  loggedInPlayer,
} from "../authentication/playerState";
import { isDefined } from "../util/isDefined";
import { makePlayer, kickPlayer, activeGameId, activeGame } from "./gameState";
import { Button } from "../components/button";

const PlayerCard = ({ player, game }: { player: Player; game: Game }) => {
  const user = useRecoilValue(loggedInPlayer);
  const isGameRunning = !!game.activeRound;
  const userIsOwner = user.id === game.ownerId;

  const playerIsOwner = player.id === game.ownerId;
  const isAdmitted = game.players.has(player.id);

  const actions = (
    <div>
      {!isAdmitted && (
        <Button
          className="w-20"
          onClick={() => makePlayer(game.id, player.id)}
          disabled={isGameRunning}
        >
          Admit
        </Button>
      )}
      <Button
        className="w-20"
        onClick={() => kickPlayer(game.id, player.id)}
        alert
        disabled={isGameRunning}
      >
        Kick
      </Button>
    </div>
  );

  return (
    <li className="flex flex-row py-2 justify-between items-center border-b border-dark-purple border-opacity-25">
      <div className="mr-4">
        <span className="font-semibold">{player.nickname}</span> (
        {playerIsOwner ? "Owner" : isAdmitted ? "Player" : "Waiting"})
      </div>
      {userIsOwner && !playerIsOwner && actions}
    </li>
  );
};

export interface LobbyProps {
  readonly game: Game;
}

export const Lobby = ({ game }: LobbyProps) => {
  const players = useRecoilValue<Array<Player | undefined>>(
    // FIXME(FP): recoil typings are broken here
    waitForAll([...game.lobby].map((id) => playerForId(id))) as any,
  );

  const user = useRecoilValue(loggedInPlayer);

  return (
    <div className="my-4">
      <h2 className="text-lg">Lobby:</h2>
      <ul>
        {players.filter(isDefined).map((it) => (
          <PlayerCard key={it.id} game={game} player={it} />
        ))}
      </ul>
    </div>
  );
};
