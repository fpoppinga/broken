import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./pages/home";
import { Game } from "./pages/game";
import { Authentication } from "./authentication/authentication";
import { GameLiveUpdate, ActivePageLiveUpdate } from "./game/gameLiveUpdate";

export const App = () => {
  return (
    <div className="flex flex-col justify-between min-h-screen w-full bg-primary">
      <header className="p-4 border-b-8 border-dark-purple border-opacity-75">
        <h1 className="text-3xl">Broken Picture Game</h1>
      </header>
      <main className="flex flex-grow flex-row justify-center">
        <Router>
          <Switch>
            <Authentication>
              <Route exact path={"/"}>
                <Home />
              </Route>
              <Route exact path={"/game/:gameId"}>
                <GameLiveUpdate />
                <ActivePageLiveUpdate />
                <Game />
              </Route>
            </Authentication>
          </Switch>
        </Router>
      </main>
      <footer className="flex flex-row justify-center">
        <span className="text-xs text-opacity-50">© 2020 - Finn Poppinga</span>
      </footer>
    </div>
  );
};

export default App;
