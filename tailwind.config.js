module.exports = {
  purge: ["./src/**/*.tsx", "./public/**/*.html"],
  theme: {
    extend: {
      colors: {
        specularity: "rgba(255, 255, 255, 0.2)",
        "maya-blue": "#55b9f3",
        "crayola-green": "#c8e9a0",
        "light-salmon": "#f7a278",
        "maroon-x11": "#a13d63",
        "dark-purple": "#351e29",
      },
      backgroundColor: {
        "maya-blue": "#55b9f3",
        "crayola-green": "#c8e9a0",
        "light-salmon": "#f7a278",
        "maroon-x11": "#a13d63",
        "dark-purple": "#351e29",
      },
      boxShadow: {
        neu:
          "-1.25rem -1.25rem 3.75rem rgba(255, 255, 255, 0.25), 1.25rem 1.25rem 3.75rem rgba(0, 0, 0, 0.15)",
      },
    },
  },
  variants: {},
  plugins: [],
};
